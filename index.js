// univasal selector
let inputbox = document.querySelector(".main-input");
let inputAndDivHolder = document.querySelector(".itemAndinputHolder");
let bottom = document.querySelector(".bottom-line");
let getItem = document.querySelector(".getElemet");
inputAndDivHolder.append(getItem);
// let arr = [];
let counting = document.querySelector(".count-Active");

// Count active task
function countlenh() {
  let hold = document.querySelectorAll(".child-holder");
  let completeCount = document.querySelectorAll(".completed");
  counting.innerText = `${hold.length - completeCount.length} item left`;
}

// Create elements in body
function createElement() {
  let creatediv = document.createElement("div");
  creatediv.classList.add("child-holder");
  creatediv.style.textAlign = "center";
  creatediv.style.overflowWrap = "break-word";

  let completeToggle = document.createElement("input");
  completeToggle.setAttribute("type", "checkbox");
  completeToggle.classList.add("toggle-btn");
  completeToggle.style.width = "30px";
  completeToggle.style.height = "30px";
  completeToggle.style.border = "1px sold black";
  creatediv.append(completeToggle);
  let para = document.createElement("p");
  para.style.paddingLeft = "50px";
  para.style.width = "80%";
  para.classList.add("not-line");
  para.innerText = inputbox.value;
  creatediv.append(para);
  getItem.append(creatediv);
  inputbox.value = ``;
  let delet = document.createElement("button");
  delet.innerText = "X";
  delet.classList.add("delet");
  creatediv.append(delet);
  bottom.style.display = "block";
  countlenh();
}

// On Click Event to create Div
inputbox.addEventListener("keydown", (e) => {
  if (e.key == "Enter" && inputbox.value) {
    createElement();
  }
});

inputAndDivHolder.addEventListener("click", function (e) {
  if (e.target.className == "toggle-btn") {
    let parent = e.target.parentElement;
    let para = parent.querySelector(".not-line");
    para.classList.toggle("lign-throught");
    parent.classList.toggle("completed");
    countlenh();
  }
  if (e.target.className == "delet") {
    e.target.parentElement.remove();
    countlenh();
  }
});

// All Button
let getAllTsk = document.querySelector(".all");
getAllTsk.addEventListener("click", () => {
  let childDiv = document.querySelectorAll(".child-holder");
  for (let i = 0; i < childDiv.length; i++) {
    childDiv[i].style.display = "block";
  }
});

// Active list
let activelist = document.querySelector(".active");
activelist.addEventListener("click", () => {
  document
    .querySelectorAll(".child-holder")
    .forEach((e) => (e.style.display = "block"));
  document.querySelectorAll(".completed").forEach((ele) => {
    ele.style.display = "none";
  });
});

// Completed list
let completedData = document.querySelector(".filCompleted");
completedData.addEventListener("click", () => {
  document
    .querySelectorAll(".child-holder")
    .forEach((e) => (e.style.display = "none"));
  document.querySelectorAll(".completed").forEach((ele) => {
    ele.style.display = "block";
  });
});

// Delete Completed Task
let clearallCompleted = document.querySelector(".clearAll");
clearallCompleted.addEventListener("click", () => {
  document.querySelectorAll(".completed").forEach((ele) => {
    ele.remove();
  });
});

// Creating Edit Text
let tem = "";
getItem.addEventListener("dblclick", (e) => {
  // e.target.match("")
  if (e.target.matches("p")) {
    // console.log(e.target.innerText);
    let inp = document.createElement("input");
    inp.classList.add("edit");
    inp.value = e.target.innerText;
    inp.style.height = "50px";
    inp.style.fontSize = "30px";
    tem = e.target.innerText;
    e.target.innerText = "";
    e.target.append(inp);
  }
  // document.querySelector(".not-line").addEventListener("input", () => {});
});
getItem.addEventListener("keyup", (e) => {
  if (e.target.matches("input") && e.key == "Enter") {
    e.target.parentElement.innerText = e.target.value;
    e.target.style.display = "none";
    console.log(e.target.value);
  }
});
